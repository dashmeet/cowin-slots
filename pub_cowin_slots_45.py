import smtplib
from email.mime.text import MIMEText
import base64
from datetime import datetime
import requests
import time
import json
from receivers import receivers_45

pune_district_id = '363' # change for other cities
cowin_email = 'slotscowin@gmail.com' # email used to send notifications, change to yours
cowin_pwd = 'xxx' # pwd for above email account
date = datetime.today()
subject = 'AVAILABLE in Pune Today %s for 45+' % date.strftime('%d %b %Y')
prev_centers_45 = []

def cowin_slots():
	url = "https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id=%s&date=%s" % (pune_district_id, date.strftime('%d-%m-%Y'))
	
	try:
		response = json.loads(requests.get(url, headers={'Cache-Control': 'no-cache', 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1 Safari/605.1.15'}).text)
		filter_responses(response)
	except:
		print("No response from server")
	
def filter_responses(response):
	centers_45 = []
	global prev_centers_45

	army_centers = ["Army", "Armed Forces", "Air Force", "ARMY", "ARMED FORCES", "AIR FORCE"]
	for center in response['centers']:
		if any(army_center in center['name'] for army_center in army_centers) or not str(center['pincode']).startswith('411'):
			continue # skip Pune outskirts and army-only centers

		for session in center['sessions']:
			if session['min_age_limit'] == 45 and session['available_capacity'] > 0:
				centers_45.append({
					'name': center['name'],
					'address': '%s - %s - %s' % (center['address'], center['block_name'], center['pincode']),
					'fee': center['fee_type'],
					'vaccine': session['vaccine'],
					'capacity': session['available_capacity'],
					'date': session['date']
				})

	if centers_45 and centers_45 != prev_centers_45:
		send_mail(centers_45, receivers_45)
		prev_centers_45 = centers_45
		print("Sent mail to above 45 group at %s" % time.strftime("%H:%M"))

def send_mail(available_centers, receivers):
	message = create_mail(available_centers)
	
	try:
		smtp_session = smtplib.SMTP('smtp.gmail.com', 587)
		smtp_session.starttls()
		smtp_session.login(cowin_email, cowin_pwd)

		text = message.as_string()
		receivers = [cowin_email] + receivers # receivers will be bcc'd
		smtp_session.sendmail(cowin_email, receivers, message.as_string())

		smtp_session.quit()
	except smtplib.SMTPException:
		print("Error: unable to send email")

def create_mail(available_centers):
	text = "<html><head><style>table {border-collapse:collapse;width:90%;} td, th {border:1px solid #ddd;text-align:left;padding:8px;} </style></head><body>"
	text += "<h3>Updated list for Today:</h3><br><br>"
	text += "<table><tr><th>Date</th> \
			<th>Name</th> \
			<th style='width:40%'>Address</th> \
			<th>Fee</th> \
			<th>Vaccine type</th> \
			<th>Available Capacity</th></tr>"

	for available_center in available_centers:
		text += "<tr><td>%s</td> \
				<td>%s</td> \
				<td>%s</td> \
				<td>%s</td> \
				<td>%s</td> \
				<td>%s</td></tr>" % (available_center['date'], available_center['name'], available_center['address'], available_center['fee'], available_center['vaccine'], available_center['capacity'])

	text += "</table><br><br> \
			<h3>Book <a href='https://selfregistration.cowin.gov.in'>NOW</a>!</h3> \
			<br>Regards,<br>Dashmeet"

	message = MIMEText(text, 'HTML')
	message['to'] = cowin_email
	message['from'] = cowin_email
	message['subject'] = subject
	
	return message

if __name__ == '__main__':
	i = 0
	log_freq = 5 * 60 	# Script running info logged every 5 mins
	script_freq = 5		# Frequency of script is 5 secs
	
	while(True):
		cowin_slots()
		
		if i % log_freq == 0:
			print("The script was running at %s" % time.strftime("%H:%M"))
		i = i + script_freq

		time.sleep(script_freq)
